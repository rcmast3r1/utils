#! /bin/sh


sudo add-apt-repository ppa:gnome-terminator
sudo apt-get update
sudo apt-get install terminator


sudo snap install --classic code

sudo apt-get install -y git xclip

read -r -p "first time? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY]) 
       	ssh-keygen -t rsa -b 2048 -C "rcmast3r1@gmail.com"
	xclip -sel clip < ~/.ssh/id_rsa.pub
	echo "CV paste into gitlab"
        ;;
    *)
        echo "ok"
        ;;
esac


read -r -p "wanna install ros? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY]) 
	sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

	sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

	sudo apt update

	sudo apt install ros-melodic-desktop-full
        ;;
    *)
        echo "ok"
        ;;
esac

sudo apt-get install -y python3-vcstool python-catkin-tools
